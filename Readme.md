TP-K8s: Etapes à suivre: 

1- Preparation de pv NFS pour Mysql :
 utiliser IP NFS: 172.16.2.56
 Path: /shared/mysql/{fateh,daouda,lynda,nora,barry,ouissem}

2- Creation de PVC de Mysql (verifier le manifest mysql pour le nom de pvc)

3- creer les secret Mysql:


kubectl create secret generic mysql-root-pass --from-literal=password=R00t


kubectl create secret generic mysql-user-pass --from-literal=username=apx --from-literal=password=server

kubectl create secret generic mysql-db-url --from-literal=database=demo --from-literal=url="jdbc:mysql://social-login-app-mysql:3306/spring?useSSL=false&serverTimezone=UTC&useLegacyDatetimeCode=false&allowPublicKeyRetrieval=true"


3- deployer Mysql.

4- après la preparation des images docker de backend vous creer le manifest et deployer le backend.

5-  après la preparation des images docker de frontend vous creer le manifest et deployer le frontend.

6- exposer vos applications via Ingress.


